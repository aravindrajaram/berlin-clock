package com.inkglobal.techtest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.inkglobal.techtest.exception.IncorrectTimeException;

public class HourLampsTest {

	@Test
	public void hourLampsDefaultTest(){
		HourLamps hourLamps = new HourLamps();
		assertEquals("OOOO OOOO", hourLamps.returnStringHours());
	}

	@Test
	public void hourLamps0() throws IncorrectTimeException {
		
		HourLamps hourLamps = new HourLamps(0);
		assertEquals("OOOO OOOO", hourLamps.returnStringHours());
	}
	
	@Test
	public void hourLamps1() throws IncorrectTimeException{
		HourLamps hourLamps = new HourLamps(1);
		assertEquals("OOOO ROOO", hourLamps.returnStringHours());
	}
	
	@Test
	public void hourLamps12() throws IncorrectTimeException{
		HourLamps hourLamps = new HourLamps(12);
		assertEquals("RROO RROO", hourLamps.returnStringHours());
	}

	@Test
	public void hourLamps24() throws IncorrectTimeException{
		HourLamps hourLamps = new HourLamps(24);
		assertEquals("RRRR RRRR", hourLamps.returnStringHours());
	}

	@Test(expected = IncorrectTimeException.class)
	public void hourLamps25() throws IncorrectTimeException {
		
		HourLamps hourLamps = new HourLamps(25);
		
	}

	@Test(expected = IncorrectTimeException.class)
	public void hourLampsNegative() throws IncorrectTimeException {
		
		HourLamps hourLamps = new HourLamps(-1);
		
	}	
}
