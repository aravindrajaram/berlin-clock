package com.inkglobal.techtest;

import static org.junit.Assert.*;

import org.junit.Test;

public class RedClockLampTest {
	
	@Test
	public void switchOnTest(){
		RedClockLamp redClockLamp = new RedClockLamp();
		redClockLamp.switchOn();
		assertEquals("R",redClockLamp.returnStringState());
	}

	@Test
	public void switchOffTest(){
		RedClockLamp redClockLamp = new RedClockLamp();
		redClockLamp.switchOff();
		assertEquals("O",redClockLamp.returnStringState());
	}

}
