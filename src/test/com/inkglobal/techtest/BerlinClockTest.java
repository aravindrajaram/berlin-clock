package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Test;

import com.inkglobal.techtest.exception.IncorrectTimeException;
import com.inkglobal.techtest.exception.IncorrectTimeFormatException;

public class BerlinClockTest {
	
	@Test
	public void berlinClockDefaultTest() {
		try {
			BerlinClock berlinClock = new BerlinClock();
			
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		
			BerlinClock berlinClock1 = new BerlinClock(dateFormat.format(Calendar.getInstance().getTime()));
			
			assertEquals(berlinClock1.getTime(), berlinClock.getTime());
		} catch (IncorrectTimeFormatException e) {
			e.printStackTrace();
		} catch (IncorrectTimeException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void berlinClockTest1() throws IncorrectTimeFormatException, IncorrectTimeException{
		BerlinClock berlinClock = new BerlinClock("12:20:00");
		
		assertEquals("Y RROO RROO YYRYOOOOOOO OOOO", berlinClock.getTime());
		
	}

	@Test
	public void berlinClockTest2() throws IncorrectTimeFormatException, IncorrectTimeException{
		BerlinClock berlinClock = new BerlinClock("17:45:03");
		
		assertEquals("O RRRO RROO YYRYYRYYROO OOOO", berlinClock.getTime());
		
	}

	//23:59:59 O RRRR RRRO YYRYYRYYRYY YYYY
	@Test
	public void berlinClockTest3() throws IncorrectTimeFormatException, IncorrectTimeException{
		BerlinClock berlinClock = new BerlinClock("23:59:59");
		
		assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY", berlinClock.getTime());
		
	}
	//13:17:01 O RROO RRRO YYROOOOOOOO YYOO
	@Test
	public void berlinClockTest4() throws IncorrectTimeFormatException, IncorrectTimeException{
		BerlinClock berlinClock = new BerlinClock("13:17:01");
		
		assertEquals("O RROO RRRO YYROOOOOOOO YYOO", berlinClock.getTime());
		
	}

	//24:00:00 Y RRRR RRRR OOOOOOOOOOO OOOO
	@Test
	public void berlinClockTest5() throws IncorrectTimeFormatException, IncorrectTimeException{
		BerlinClock berlinClock = new BerlinClock("24:00:00");
		
		assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO", berlinClock.getTime());
		
	}
	//06:07:00 Y ROOO ROOO YOOOOOOOOOO YYOO
	@Test
	public void berlinClockTest6() throws IncorrectTimeFormatException, IncorrectTimeException{
		BerlinClock berlinClock = new BerlinClock("06:07:00");
		
		assertEquals("Y ROOO ROOO YOOOOOOOOOO YYOO", berlinClock.getTime());
		
	}}
