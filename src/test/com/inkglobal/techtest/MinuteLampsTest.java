package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.inkglobal.techtest.exception.IncorrectTimeException;

public class MinuteLampsTest {
	
	@Test
	public void minuteLampsDefaultTest(){
		MinuteLamps minuteLamps = new MinuteLamps();
		assertEquals("OOOOOOOOOOO OOOO", minuteLamps.returnStringMinutes());
	}

	@Test
	public void minuteLamps0() throws IncorrectTimeException{
		MinuteLamps minuteLamps = new MinuteLamps(0);
		assertEquals("OOOOOOOOOOO OOOO", minuteLamps.returnStringMinutes());
	}

	@Test
	public void minuteLamps1() throws IncorrectTimeException{
		MinuteLamps minuteLamps = new MinuteLamps(1);
		assertEquals("OOOOOOOOOOO YOOO", minuteLamps.returnStringMinutes());
	}

	@Test
	public void minuteLamps59() throws IncorrectTimeException{
		MinuteLamps minuteLamps = new MinuteLamps(59);
		assertEquals("YYRYYRYYRYY YYYY", minuteLamps.returnStringMinutes());
	}
	
	@Test(expected = IncorrectTimeException.class)
	public void minuteLamps60() throws IncorrectTimeException{
		MinuteLamps minuteLamps = new MinuteLamps(60);
		//assertEquals("OOOOOOOOOOO YOOO", minuteLamps.returnStringMinutes());
	}
	@Test(expected = IncorrectTimeException.class)
	public void minuteLampsNegative() throws IncorrectTimeException{
		MinuteLamps minuteLamps = new MinuteLamps(-1);
		//assertEquals("OOOOOOOOOOO YOOO", minuteLamps.returnStringMinutes());
	}
}
