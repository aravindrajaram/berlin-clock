package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class YellowClockLampTest {
	
	@Test
	public void switchOnTest(){
		YellowClockLamp yellowClockLamp = new YellowClockLamp();
		yellowClockLamp.switchOn();
		assertEquals("Y",yellowClockLamp.returnStringState());
	}

	@Test
	public void switchOffTest(){
		YellowClockLamp yellowClockLamp = new YellowClockLamp();
		yellowClockLamp.switchOff();
		assertEquals("O",yellowClockLamp.returnStringState());
	}	

}
