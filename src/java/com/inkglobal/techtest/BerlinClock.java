package com.inkglobal.techtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inkglobal.techtest.exception.IncorrectTimeException;
import com.inkglobal.techtest.exception.IncorrectTimeFormatException;

/**
 * The Berlin Uhr (Clock) is a rather strange way to show the time. 
 * 
 * On the top of the clock there is a yellow lamp that blinks on/off every two seconds. 
 * The time is calculated by adding rectangular lamps. 
 * 
 * The top two rows of lamps are red. 
 * These indicate the hours of a day. In the top row there are 4 red lamps. Every lamp represents 5 hours. 
 * In the lower row of red lamps every lamp represents 1 hour. So if two lamps of the first row and three
 * of the second row are switched on that indicates 5+5+3=13h or 1 pm. 
 * 
 * The two rows of lamps at the bottom count the minutes. 
 * The first of these rows has 11 lamps, the second 4. In the first row every lamp represents 5 minutes. 
 * In this first row the 3rd, 6th and 9th lamp are red and indicate the first quarter, half and last quarter of an hour. 
 * The other lamps are yellow. In the last row with 4 lamps every lamp represents 1 minute.
 * 
 * The lamps are switched on from left to right.
 * 
 * @author Aravind Rajaram
 * 
 */
public class BerlinClock {

	ClockLamp secondsLamp;
	
	HourLamps hourLamps;
	
	MinuteLamps minuteLamps;
	
	/**
	 * Default Constructor sets the clock to current time.
	 *
	 */
	public BerlinClock() {
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		
		try {
			this.setTime(dateFormat.format(Calendar.getInstance().getTime()));
		} catch (IncorrectTimeFormatException e) {
			e.printStackTrace();
		} catch (IncorrectTimeException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Constructor that takes a String parameter and sets clock time to the given value.
	 * 
	 * @param strTime String representation of time in the format "hh:mm:ss"
	 * @throws IncorrectTimeFormatException
	 * @throws IncorrectTimeException
	 */
	public BerlinClock(String strTime) throws IncorrectTimeFormatException, IncorrectTimeException {
		
		this.setTime(strTime);
		
	}
	
	/**
	 * Private method that sets the berlin-clock with the given time.
	 *  
	 * @param strTime String representation of time in the format "hh:mm:ss"
	 * @throws IncorrectTimeFormatException
	 * @throws IncorrectTimeException
	 */
	private void setTime(String strTime) throws IncorrectTimeFormatException, IncorrectTimeException {
		
		int hours = 0, minutes = 0, seconds = 0;
		
		final String REGEX = "(\\d{2}):(\\d{2}):(\\d{2})";
		
		Matcher matcher = Pattern.compile(REGEX).matcher(strTime);
		
		if(matcher.find()) {
			hours = Integer.valueOf(matcher.group(1)).intValue();
			minutes = Integer.valueOf(matcher.group(2)).intValue();
			seconds = Integer.valueOf(matcher.group(3)).intValue();
		} else {
			throw new IncorrectTimeFormatException();
		}
		
		if(seconds < 0 || seconds > 59) {
			throw new IncorrectTimeException("Seconds value is out of expected range[0 - 59].");
		}
		secondsLamp = new YellowClockLamp();
		if(seconds % 2 == 0){
			secondsLamp.switchOn();
		} else {
			secondsLamp.switchOff();			
		}
		
		hourLamps = new HourLamps(hours);
		
		minuteLamps = new MinuteLamps(minutes);
	}
	
	/**
	 * @return berlin-clock string representation of the given time.
	 */
	public String getTime() {
		
		StringBuffer time = new StringBuffer();
		
		time.append(secondsLamp.returnStringState());
		time.append(" ");
		time.append(hourLamps.returnStringHours());
		time.append(" ");
		time.append(minuteLamps.returnStringMinutes());
		
		return time.toString();		
	}

}