package com.inkglobal.techtest.exception;

public class IncorrectTimeFormatException extends Exception {

	/**
	 * Default Constructor
	 */
	public IncorrectTimeFormatException() {
		
	}
	
	/**
	 * Constructor with a parameter.
	 * 
	 * @param msg
	 */
	public IncorrectTimeFormatException(String msg) {
		super(msg);
	}
}
