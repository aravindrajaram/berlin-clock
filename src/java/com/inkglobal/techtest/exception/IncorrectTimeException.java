package com.inkglobal.techtest.exception;

/**
 * This exception is thrown when Hours/Minutes/Seconds has an incorrect value.
 * e.g., hours == 25 will cause this exception to be thrown.
 * 
 * @author Aravind Rajaram
 *
 */
public class IncorrectTimeException extends Exception {
	
	/**
	 * Default Constructor
	 */
	public IncorrectTimeException() {
		
	}
	
	/**
	 * Constructor with a parameter to convey a message.
	 * 
	 * @param msg
	 */
	public IncorrectTimeException(String msg) {
		super(msg);
	}

}
