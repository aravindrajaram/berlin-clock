package com.inkglobal.techtest;

import com.inkglobal.techtest.exception.IncorrectTimeException;

/**
 * MinuteLamps.java
 * 
 * Representation of the two minute rows of berlin-clock.
 * Row 1: YYYRYYYRYYYRYYY
 * Row 2: YYYY
 * 
 * Each Yellow lamp in row1 represents 5 minutes and Each Red lamp represents quarter of an hour.
 * Each Yellow lamp in row2 represents 1 minute.
 * 
 * @author Aravind Rajaram
 *
 */
public class MinuteLamps {
	
	ClockLamp minuteLampRow1[];
	ClockLamp minuteLampRow2[];
	
	/**
	 * Default Constructor
	 * 
	 */
	public MinuteLamps(){
		
		minuteLampRow1 = new ClockLamp[11];
		minuteLampRow2 = new ClockLamp[4];
		
		for(int ctr = 0; ctr < minuteLampRow1.length; ctr++){
			if((ctr + 1)%3 == 0){
				minuteLampRow1[ctr] = new RedClockLamp();
			} else {				
				minuteLampRow1[ctr] = new YellowClockLamp();
			}
		}

		for(int ctr = 0; ctr < minuteLampRow2.length; ctr++){
			minuteLampRow2[ctr] = new YellowClockLamp();
		}

	}

	/**
	 * Constructor with a parameter
	 * 
	 * @param minutes
	 * @throws IncorrectTimeException
	 */
	public MinuteLamps(int minutes) throws IncorrectTimeException {
			
		this();

		if(minutes < 0 || minutes > 59)
			throw new IncorrectTimeException("Minutes value is out of expected range[0 - 59].");

		int row1ctr = minutes / 5;
		int row2ctr = minutes % 5;
		
		for(int ctr = 0;ctr < row1ctr; ctr++ ){
			minuteLampRow1[ctr].switchOn();
		}
		
		for(int ctr = 0;ctr < row2ctr; ctr++ ){
			minuteLampRow2[ctr].switchOn();
		}		
	}

	/**
	 * Returns berlin-clock representation of minutes part.
	 * 
	 * @return
	 */
	public String returnStringMinutes() {
		StringBuffer strMinutes = new StringBuffer("");
		
		for(ClockLamp lamp: minuteLampRow1){
			strMinutes.append(lamp.returnStringState());
		}
		
		strMinutes.append(" ");
		
		for(ClockLamp lamp: minuteLampRow2){
			strMinutes.append(lamp.returnStringState());
		}
		return strMinutes.toString();
	}
}
