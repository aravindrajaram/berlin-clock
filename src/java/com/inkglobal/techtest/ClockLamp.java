package com.inkglobal.techtest;

/**
 * Abstract ClockLamp class which forms the base class for all ClockLamp classes.
 * 
 * @author Aravind Rajaram
 *
 */

public abstract class ClockLamp {

	/** 
	 * false == Off 
	 * true  == On
	 */
	protected boolean lampState = false;
	
	/**
	 * Turn On the lamp.
	 */
	public void switchOn(){
		this.lampState = true;
	}
	/**
	 * Turn Off the lamp.
	 */
	public void switchOff() {
		this.lampState = false;
	}
	
	/**
	 * Abstract method to return lampState in String format. To be overidden in Child class.
	 * e.g., RedClockLamp when switched on will return "R". All ClockLamps will return "O" if switched off.
	 * 
	 * @return "O" for Switched off. "R" for RedClockLamp turned on. "Y" for YellowClockLamp turned on.
	 */
	abstract public String returnStringState();
}