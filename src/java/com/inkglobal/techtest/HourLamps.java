package com.inkglobal.techtest;

import com.inkglobal.techtest.exception.IncorrectTimeException;

/**
 * Representation of the two hour rows of berlin-clock.
 * Row 1: RRRR
 * Row 2: RRRR
 * 
 * Each Red lamp in row1 represents 5 hours.
 * Each Red lamp in row2 represents 1 hour.
 * 
 * @author Aravind Rajaram
 *
 */
public class HourLamps {

	ClockLamp hourLampRow1[];
	
	ClockLamp hourLampRow2[];
	
	/**
	 * Default Constructor
	 */
	public HourLamps() {

		hourLampRow1 = new ClockLamp[4];
		hourLampRow2 = new ClockLamp[4];
		
		for(int ctr = 0; ctr < hourLampRow1.length; ctr++){
			hourLampRow1[ctr] = new RedClockLamp();
			hourLampRow2[ctr] = new RedClockLamp();
		}
	}
	
	/**
	 * Constructor with parameter
	 * @param hours
	 * @throws IncorrectTimeException
	 */
	public HourLamps(int hours) throws IncorrectTimeException {
		this();
		
		if(hours < 0 || hours > 24) {
			throw new IncorrectTimeException("Hours value is out of expected range[0 - 24].");
		}
			
		int row1ctr = hours / 5;
		int row2ctr = hours % 5;
		
		for(int ctr = 0;ctr < row1ctr; ctr++ ){
			hourLampRow1[ctr].switchOn();
		}
		
		for(int ctr = 0;ctr < row2ctr; ctr++ ){
			hourLampRow2[ctr].switchOn();
		}
	}
	/**
	 * Returns berlin-clock representation of hours part.
	 * 
	 * @return
	 */	
	public String returnStringHours() {
		StringBuffer strHours = new StringBuffer("");
		
		for(ClockLamp lamp: hourLampRow1){
			strHours.append(lamp.returnStringState());
		}
		
		strHours.append(" ");
		
		for(ClockLamp lamp: hourLampRow2){
			strHours.append(lamp.returnStringState());
		}
		return strHours.toString();
	}
	
}
