package com.inkglobal.techtest;

/**
 * RedClockLamp implementation class.
 * 
 * @author Aravind Rajaram
 *
 */
public class RedClockLamp extends ClockLamp {

	/* (non-Javadoc)
	 * @see com.inkglobal.techtest.ClockLamp#returnStringState()
	 */
	@Override
	public String returnStringState() {
		return this.lampState? "R": "O";
	}
}
