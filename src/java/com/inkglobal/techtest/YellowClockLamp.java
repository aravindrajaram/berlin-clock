package com.inkglobal.techtest;

/**
 * YellowClockLamp implementation class.
 * 
 * @author Aravind Rajaram
 *
 */
public class YellowClockLamp extends ClockLamp {

	/* (non-Javadoc)
	 * @see com.inkglobal.techtest.ClockLamp#returnStringState()
	 */
	@Override
	public String returnStringState() {
		return this.lampState? "Y": "O";
	}

}
